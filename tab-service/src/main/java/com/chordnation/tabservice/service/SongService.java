package com.chordnation.tabservice.service;

import com.chordnation.tabservice.model.Song;
import com.chordnation.tabservice.repository.SongRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SongService {
    private final SongRepository songRepository;

    public SongService(SongRepository songRepository) {
        this.songRepository = songRepository;
    }

    public List<Song> getAllSongs(){
        return songRepository.findAll();
    }

    public void addSong(Song song){
        songRepository.save(song);
    }

    public void deleteSong(Long id){
        songRepository.deleteById(id);
    }
}
