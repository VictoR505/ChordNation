package com.chordnation.tabservice.model.enums;

public enum Level {
    BEGINNER, INTERMEDIATE, ADVANCED, MASTER;
}
