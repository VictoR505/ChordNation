package com.chordnation.tabservice.model.enums;

public enum TabType {
    CHORD, TAB;
}
