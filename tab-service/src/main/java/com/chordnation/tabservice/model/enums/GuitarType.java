package com.chordnation.tabservice.model.enums;

public enum GuitarType {
    ELECTRIC, ACOUSTIC;
}
